# -*- coding: utf-8 -*-

from __future__ import print_function

import matplotlib
matplotlib.use('Agg')

import numpy as np

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import training, optimizers, serializers
from chainer.training import extensions
from chainer.datasets import tuple_dataset
from chainer.links import caffe

import pandas as pd
from PIL import Image
import random
import argparse

crop_size = 224
max_image_size = 480 
min_image_size = 256
train_size = 9000
labeled_size = 10000
test_size = 10000
unlabeled_size = 50000
# unlabeled_size = 20000
eps = 1.0e-5

def change_data(image, use_random):
    w, h = image.size
    shrink_rate = random.uniform(min_image_size + eps, max_image_size) / min(w, h)
    nw = int(w * shrink_rate)
    nh = int(h * shrink_rate)
    image = image.resize((nw, nh), Image.ANTIALIAS)
    # need anti-alias?

    if use_random:
        # Randomly crop a region and flip the image
        top = random.randint(0, nh - crop_size - 1)
        left = random.randint(0, nw - crop_size - 1)
    else:
        # Crop the center
        top = (nh - crop_size) // 2
        left = (nw - crop_size) // 2
        
    bottom = top + crop_size
    right = left + crop_size

    image = np.array(image.crop((left, top, right, bottom)), dtype=np.float32)        

    # add noise
    uniform_noize = np.random.uniform(0.98, 1.02, image.shape)
    image *= uniform_noize

    # singleチャネルなら、3チャネルに複製
    if len(image.shape) == 2:
        _image = image
        w, h = image.shape
        image = np.zeros((w, h, 3),dtype=np.float32)
        for i in range(3):
	    image[:,:,i] = _image

    # 3チャネル以上なら、3チャネルにする
    if image.shape[0] > 3:
	image = image[:,:,:3]

    # global contrast normalization
    mean = np.mean(image, axis=(0, 1))
    var  = np.sqrt(np.var(image, axis=(0, 1)))
    # unlabeled_18423, 5502, 46944

    # np.array: for both single/multi channel data
    if min(abs(var)) == 0:
        image = (image - mean).transpose(2, 0, 1)
    else:
        image = ((image - mean) / var).transpose(2, 0, 1)

    # 左右反転
    if use_random and random.randint(0, 1):
        image = image[:, :, ::-1]

    return image

def get_unlabeled_image_path(i):
    return "clf_train_images_unlabeled_%d/unlabeled_%d.jpg" % (i / 5000 + 1, i)

def get_labeled_image_path(i):
    return "clf_train_images_labeled/train_%d.jpg" % i

def get_test_image_path(i):
    return "clf_test_images/test_%d.jpg" % i

def get_labeled_csv_filename():
    return "clf_train_master.tsv"

# scale augmentation, horizontal filp
class PreprocessedDataset(chainer.dataset.DatasetMixin):

    def __init__(self, path_labels, random=True):
	self.path_labels = path_labels
	self.random = random

    def __len__(self):
        return len(self.path_labels)

    def get_example(self, i):
        # 画像を1つずつ読み込み
	path, label = self.path_labels[i]
        return change_data(Image.open(path), self.random), label

# scale augmentation, horizontal filp
class UnlabeledPreprocessedDataset(chainer.dataset.DatasetMixin):

    def __init__(self, image_path, random=True):
        self.image_path = image_path
        self.random = random

    def __len__(self):
        return len(self.image_path)

    def get_example(self, i):
        # 画像を1つずつ読み込み
	return change_data(Image.open(self.image_path[i]), self.random)
        
def read_cookpad_dataset(home_path, size, getter_image, getter_label = None):

    image_path_list = ["%s/%s" % (home_path, getter_image(i)) for i in range(size)]
    
    if getter_label == None:
        return image_path_list
    else:
        # pandasで正解ラベルを読み込み
        label_path = "%s/%s" % (home_path, getter_label())
        labels = np.array(list(pd.read_csv(label_path, sep='\t')['category_id']), dtype=np.int32)
        
        return zip(image_path_list, labels)

def get_cookpad_dataset(home_path):
    base = read_cookpad_dataset(home_path, labeled_size, get_labeled_image_path, get_labeled_csv_filename)

    random.shuffle(base)

    return PreprocessedDataset(base[:train_size]), PreprocessedDataset(base[train_size:])

class Alex(chainer.Chain):
    def __init__(self, no_label):

        self.train = True
        
        super(Alex, self).__init__(
            conv1=L.Convolution2D(None, 188, 7, stride=2),
            bn1=L.BatchNormalization(188),
            conv15=L.Convolution2D(None,  256, 5, pad=2),
            bn15=L.BatchNormalization(256),
            conv2=L.Convolution2D(None, 256,  5, stride=2, pad=2),
            bn2=L.BatchNormalization(256),
            conv3=L.Convolution2D(None, 384,  3, pad=1),
            bn3=L.BatchNormalization(384),
            conv4=L.Convolution2D(None, 384,  3, pad=1),
            bn4=L.BatchNormalization(384),
            conv5=L.Convolution2D(None, 256,  3, pad=1),
            bn5=L.BatchNormalization(256),
            fc7=L.Linear(None, 1024),
	    bn7=L.BatchNormalization(1024),
            fc8=L.Linear(None, no_label),
        )

    def __call__(self, x):
        h = F.max_pooling_2d(
            F.relu(self.bn1(self.conv1(x), test=not self.train)), 3, stride=2)
        h = F.relu(self.bn15(self.conv15(h), test=not self.train))
        h = F.max_pooling_2d(
            F.relu(self.bn2(self.conv2(h), test=not self.train)), 3, stride=2)
        h = F.relu(self.bn3(self.conv3(h), test=not self.train))
        h = F.relu(self.bn4(self.conv4(h), test=not self.train))
        h = F.max_pooling_2d(F.relu(self.bn5(self.conv5(h), test=not self.train)), 3, stride = 2)
        h = F.relu(self.bn7(self.fc7(h), test=not self.train))
        y = self.fc8(h)

        return y

class TestModeEvaluator(extensions.Evaluator):

    def evaluate(self):
        model = self.get_target('main')
        model.train = False
        ret = super(TestModeEvaluator, self).evaluate()
        model.train = True
        return ret

def get_parser():
    parser = argparse.ArgumentParser("DNN for cookpad contest")

    parser.add_argument('--batchsize', '-b', type=int, default=20,
                        help="Number of images in each mini-batch")
    parser.add_argument('--epoch', '-e', type=int, default=200,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--gpu', '-g', type=int, default=-1,
                        help='GPU ID (negative value indicates CPU)')
    parser.add_argument('--out', '-o', default='result',
                        help='Directory to output the result')
    parser.add_argument('--resume', '-r', default='',
                        help='Resume the training from snapshot')
    parser.add_argument('--data', '-d', default='',
                        help='path for accessing dataset')
    parser.add_argument('--weight', '-w', type=int, default=8,
                        help='weight decay of L1 norm')
    parser.add_argument('--B1', '-B', type=float, default=0.9,
                        help='learning rate for Adam')
    parser.add_argument('--maxsize', '-mx', type=int, default=480,
                        help='max image size')
    return parser

# ====================
# TODO: rewrite it by myself

def predict(test_data, classifier, gpu = True):
    if gpu:
        classifier.predictor.to_gpu()
    else:
        classifier.predictor.to_cpu()
        
    classifier.predictor.train = False
    predictions = np.zeros((len(test_data),25))

    for i in range(len(test_data)):
        data = test_data.get_example(i).copy()
        data.resize((1, 3, crop_size, crop_size))
        x = chainer.Variable(data)
        if gpu:
            x.to_gpu()

        yhat = classifier.predictor(x)
        yhat = F.softmax(yhat)
        yhat.to_cpu()
        predictions[i,:] = yhat.data
        if i % 1000 == 0:
            print(i)

    return predictions
# ====================

def copy_model(src, dst):
    assert isinstance(src, chainer.Chain)
    assert isinstance(dst, chainer.Chain)
    for child in src.children():
        if child.name not in dst.__dict__: continue
        dst_child = dst[child.name]
        if type(child) != type(dst_child): continue
        if isinstance(child, chainer.Chain):
            copy_model(child, dst_child)
        if isinstance(child, chainer.Link):
            match = True
            for a, b in zip(child.namedparams(), dst_child.namedparams()):
                if a[0] != b[0]:
                    match = False
                    break
                if a[1].data.shape != b[1].data.shape:
                    match = False
                    break
            if not match:
                print ('Ignore %s because of parameter mismatch' % child.name)
                continue
            for a, b in zip(child.namedparams(), dst_child.namedparams()):
                b[1].data = a[1].data
            print ('Copy %s' % child.name)

def pretrain():
    home_path = "/ramdisk/data/cookpad"

    no_label = unlabeled_size
    
    parser = get_parser()
    args = parser.parse_args()
    
    alex = Alex(no_label)

    if args.gpu >= 0:
	alex.to_gpu() 
    model = L.Classifier(alex)
    
    global max_image_size
    max_image_size = args.maxsize
    print(max_image_size)
    assert max_image_size > min_image_size

    if args.gpu >= 0:
        chainer.cuda.get_device(args.gpu).use()
        model.to_gpu()

    optimizer = optimizers.Adam(beta1=0.6)
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.Lasso(1e-8))
    
    train_data = read_cookpad_dataset(home_path, unlabeled_size, get_unlabeled_image_path)
    train_label = [np.int32(i) for i in range(unlabeled_size)]
    data_label = PreprocessedDataset(zip(train_data, train_label))

    train_iter = chainer.iterators.SerialIterator(data_label, args.batchsize)

    pretrain_epoch = 100
    
    updater = training.StandardUpdater(train_iter, optimizer, device=args.gpu)
    trainer = training.Trainer(updater, (pretrain_epoch, 'epoch'), out=args.out)

    trainer.extend(extensions.snapshot(filename='snapshot_pretrain_iter_{.updater.iteration}'), trigger=(pretrain_epoch / 5, 'epoch'))
    trainer.extend(extensions.LogReport())
    trainer.extend(
        extensions.PlotReport(['main/loss'],
                              'epoch', file_name='loss_pretrain.png'))
    trainer.extend(
        extensions.PlotReport(['main/accuracy'],
                              'epoch', file_name='accuracy_pretrain.png'))
    trainer.extend(extensions.PrintReport(['epoch', 'main/loss',
                                           'main/accuracy', 'elapsed_time']))
    trainer.extend(extensions.ProgressBar(update_interval=5))

    serializers.load_npz('snapshot_pretrain_iter_40000', trainer)
    # trainer.run()
    
    return alex

def main():
    home_path = "/ramdisk/data/cookpad"

    no_label = 25
    
    parser = get_parser()
    args = parser.parse_args()
    
    print('GPU: {}'.format(args.gpu))
    print('# Minibatch-size: {}'.format(args.batchsize))
    print('# epoch: {}'.format(args.epoch))
    
    pretrained_alex = pretrain()
    
    alex = Alex(no_label)
    copy_model(pretrained_alex, alex)
    
    if args.gpu >= 0:
	alex.to_gpu() 
    model = L.Classifier(alex)
    
    global max_image_size
    max_image_size = args.maxsize
    print(max_image_size)
    assert max_image_size > min_image_size

    if args.gpu >= 0:
        chainer.cuda.get_device(args.gpu).use()
        model.to_gpu()

    optimizer = optimizers.Adam(beta1=0.6)
    optimizer.setup(model)
    optimizer.add_hook(chainer.optimizer.Lasso(1e-8))
    
    # train, test = chainer.datasets.get_cifar10()
    train, test = get_cookpad_dataset(home_path)

    train_iter = chainer.iterators.SerialIterator(train, args.batchsize)
    test_iter = chainer.iterators.SerialIterator(test, args.batchsize, repeat=False, shuffle=False)

    updater = training.StandardUpdater(train_iter, optimizer, device=args.gpu)
    # args.out = "result_mx%d" % args.maxsize
    trainer = training.Trainer(updater, (args.epoch, 'epoch'), out=args.out)

    trainer.extend(TestModeEvaluator(test_iter, model, device=args.gpu))
    trainer.extend(extensions.dump_graph('main/loss'))
    trainer.extend(extensions.snapshot(), trigger=(args.epoch / 5, 'epoch'))
    trainer.extend(extensions.LogReport())
    
    trainer.extend(
        extensions.PlotReport(['main/loss', 'validation/main/loss'],
                              'epoch', file_name='loss.png'))
    trainer.extend(
        extensions.PlotReport(['main/accuracy', 'validation/main/accuracy'],
                              'epoch', file_name='accuracy.png'))
    trainer.extend(extensions.PrintReport(['epoch', 'main/loss', 'validation/main/loss',
                                           'main/accuracy', 'validation/main/accuracy', 'elapsed_time']))
    trainer.extend(extensions.ProgressBar(update_interval=5))
    
    if args.resume:
        chainer.serializers.load_npz(args.resume, trainer)

    trainer.run()
    # ====================
    # eval
    test_base = UnlabeledPreprocessedDataset(read_cookpad_dataset(home_path, test_size, get_test_image_path))
    p = predict(test_base, model)
    pd.DataFrame(p.argmax(axis=1)).to_csv(home_path + '/submit.csv', header=None)
    
if __name__ == "__main__":
    main()
