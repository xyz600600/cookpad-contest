# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import os
import six
import chainer
import chainer.functions as F
import chainer.links as L
import zipfile
from chainer.links import caffe
from chainer import link, Chain, optimizers, Variable, training, serializers
from chainer.training import extensions
from PIL import Image
import random
from numpy.random import normal 
import time
import argparse
import math
import cupy

# implement data augmentation
def get_changed_image(img, crop_size, rand):
    max_image_size = 480
    min_image_size = 256
    eps = 1.0e-5
    
    w, h = img.size
    shrink_rate = random.uniform(min_image_size + eps, max_image_size) / min(w, h) 
    nw = int(w * shrink_rate)
    nh = int(h * shrink_rate)
    img.thumbnail((nw, nh))

    if rand:
        top = random.randint(0, nh - crop_size - 1)
        left = random.randint(0, nw - crop_size - 1)
    else:
	top = (nh - crop_size) // 2
        left = (nw - crop_size) // 2

    img = img.rotate(random.randint(-10, 10))

    bottom = top + crop_size
    right = left + crop_size

    img = np.array(img.crop((left, top, right, bottom)), dtype=np.float32)

    # global contrast normalization
    mean = np.mean(img, axis=(0, 1))
    var  = np.sqrt(np.var(img, axis=(0, 1)))
    # unlabeled_18423, 5502, 46944

    # np.array: for both single/multi channel data
    if min(abs(var)) == 0:
        img = (img - mean).transpose(2, 0, 1)
    else:
        img = ((img - mean) / var).transpose(2, 0, 1)

    if rand and random.randint(0, 1):
        img = img[:,:,::-1]

    # add noise
    uniform_noize = np.random.uniform(0.98, 1.02, img.shape)
    img *= uniform_noize
        
    return img

class ImageData():
    def __init__(self, img_dir, meta_data):
        self.img_dir = img_dir

        assert meta_data.endswith('.tsv')
        self.meta_data = pd.read_csv(meta_data, sep='\t')
        self.index = np.array(self.meta_data.index)
        self.split = False

    def shuffle(self):
        assert self.split == True
        self.train_index = np.random.permutation(self.train_index)

    def split_train_val(self, train_size):
        self.train_index = np.random.choice(self.index, train_size, replace=False)
        self.val_index = np.array([i for i in self.index if i not in self.train_index])
        self.split = True

    def generate_minibatch(self, batchsize, img_size = 227, mode = None):
        if mode == 'train':
            assert self.split == True
            meta_data = self.meta_data.ix[self.train_index]
            index = self.train_index

        elif mode == 'val':
            assert self.split == True
            meta_data = self.meta_data.ix[self.val_index]
            index = self.val_index

        else:
            meta_data = self.meta_data
            index = self.index
            
        i = 0
        while i < len(index):
            data = meta_data.iloc[i:i + batchsize]
            images = []
            for f in list(data['file_name']):
                image = Image.open(os.path.join(self.img_dir, f))
                image = get_changed_image(image, img_size, True)
                images.append(image)
            images = np.array(images)

            if 'category_id' in data.columns:
                labels = np.array(list(data['category_id']))
                labels = labels.astype(np.int32)
                yield images, labels
            else:
                yield images
            i += batchsize

class Alex(Chain):
    def __init__(self, out_size):

        self.train = True
        super(Alex, self).__init__(
            conv1=L.Convolution2D(3,  96, 11, stride=4),
            conv2=L.Convolution2D(96, 256,  5, pad=2),
            conv3=L.Convolution2D(256, 384,  3, pad=1),
            conv4=L.Convolution2D(384 , 384,  3, pad=1),
            conv5=L.Convolution2D(384, 256,  3, pad=1),
            fc6=L.Linear(9216, 4096),
            fc7=L.Linear(4096, 4096),
            fc8=L.Linear(4096, out_size),
        )

    def __call__(self, x):
        h = F.max_pooling_2d(F.local_response_normalization(
            F.relu(self.conv1(x))), 3, stride=2)
        h = F.max_pooling_2d(F.local_response_normalization(
            F.relu(self.conv2(h))), 3, stride=2)
        h = F.relu(self.conv3(h))
        h = F.relu(self.conv4(h))
        h = F.max_pooling_2d(F.relu(self.conv5(h)), 3, stride = 2)
        h = F.dropout(F.relu(self.fc6(h)), train=self.train)
        h = F.dropout(F.relu(self.fc7(h)), train=self.train)
        y = self.fc8(h)

        return y

def compute_kld(p, q):
    return F.reshape(F.sum(p * (F.log(p + 1e-16) - F.log(q + 1e-16)), axis=1), (-1, 1))

def get_unit_vector(v):
    v /= (np.sqrt(np.sum(v ** 2, axis=1)).reshape((-1, 1)) + 1e-16)
    return v
    
def calculate_vat_loss(x, predictor, xi = 10.0, eps=1.0):

    predictor.train = False
    y1 = F.softmax(predictor(x))
    d = Variable(cupy.random.normal(size=x.shape, dtype=np.float32))

    for i in range(1):
        y2 = F.softmax(predictor(x + d * xi))
        kld = F.sum(compute_kld(y1, y2))
        
        d_grad = d.grad
        print d_grad
        print cupy.linalg.norm(d_grad, axis=0).shape
        
        d = Variable(cupy.linalg.norm(d_grad + 1e-16, axis=(1,2,3)))

    y2 = F.softmax(predictor(x + eps * d))
    return -compute_kld(y1, y2)
        
def train_val(train_data, classifier, optimizer, num_train = 9000, epochs = 200, batchsize = 30, gpu = True):
    # split data to train and val
    train_data.split_train_val(num_train)

    min_validation_loss = 1e100
    continuous_non_update_time = 0
    
    for epoch in range(epochs):
        start_time = time.clock()
        eps = 8e-2
        
        # train
        classifier.predictor.train = True
        num_samples = 0
        train_cum_loss = 0
        train_cum_acc = 0
	counter = 0
        for data in train_data.generate_minibatch(batchsize, mode = 'train'):
	    counter += 1
	    if counter % 10 == 0:
		print "counter = ", counter

            num_samples += len(data[0])

            x, y = Variable(data[0]), Variable(data[1])

            if gpu:
                x.to_gpu()
                y.to_gpu()
                
	    # first loop
            loss = classifier(x, y)
            optimizer.zero_grads()
            loss.backward()    # back propagation

	    # second loop
            z = eps * cupy.sign(x.grad)
            z_val = Variable(x.data + z)
            loss = classifier(z_val, y)
            optimizer.zero_grads()
            loss.backward()    # back propagation

            optimizer.update() # update parameters

            # vat_loss = calculate_vat_loss(x, classifier.predictor)
            # print vat_loss
            # optimizer.zero_grads()
            # vat_loss.backward()
            # optimizer.update()
            
            # loss.unchain_backward()
            # vat_loss.unchain_backward()
            
            train_cum_acc += classifier.accuracy.data*batchsize
            train_cum_loss += classifier.loss.data*batchsize

        train_accuracy = train_cum_acc/num_samples
        train_loss = train_cum_loss/num_samples

        # validation
        classifier.predictor.train = False
        num_samples = 0
        val_cum_loss = 0
        val_cum_acc = 0
        for data in train_data.generate_minibatch(batchsize, mode = 'val'):
            num_samples += len(data[0])
            x, y = Variable(data[0]), Variable(data[1])
            if gpu:
                x.to_gpu()
                y.to_gpu()
            loss = classifier(x, y)

            val_cum_acc += classifier.accuracy.data*batchsize
            val_cum_loss += classifier.loss.data*batchsize

        val_accuracy = val_cum_acc/num_samples
        val_loss = val_cum_loss/num_samples

        print val_loss, min_validation_loss
        if val_loss < min_validation_loss:
            continuous_non_update_time = 0
            min_validation_loss = val_loss
        else:
            continuous_non_update_time += 1
        if continuous_non_update_time == 10:
            continuous_non_update_time = 0
            optimizer.beta1 *= .5
        print continuous_non_update_time
        end_time = time.clock()
        
        print '-----------------', 'epoch:', epoch+1, '-----------------'
        print 'train_accuracy:', train_accuracy, 'train_loss:', train_loss
        print 'val_accuracy:', val_accuracy, 'val_loss:', val_loss
        print 'elapsed time: ', (end_time - start_time), '[s]'
        print '\n'
        
        train_data.shuffle()

    return classifier, optimizer

def predict(test_data, classifier, batchsize = 30, gpu = True):
    if gpu:
        classifier.predictor.to_gpu()
    else:
        classifier.predictor.to_cpu()
    classifier.predictor.train = False
    num_samples = 0
    predictions = np.zeros((len(test_data.index),25))
    for data in test_data.generate_minibatch(batchsize):
        num_samples += len(data)
        #print num_samples, '/', len(test_data.index)
        x = Variable(data)
        if gpu:
            x.to_gpu()
        yhat = classifier.predictor(x)
        yhat = F.softmax(yhat)
        yhat.to_cpu()
        predictions[num_samples-len(data):num_samples,:] = yhat.data

    return predictions

# ====================
class BottleNeckA(chainer.Chain):
    def __init__(self, in_size, ch, out_size, stride=2):
        w = math.sqrt(2)
        super(BottleNeckA, self).__init__(
            conv1=L.Convolution2D(in_size, ch, 1, stride, 0, w, nobias=True),
            bn1=L.BatchNormalization(ch),
            conv2=L.Convolution2D(ch, ch, 3, 1, 1, w, nobias=True),
            bn2=L.BatchNormalization(ch),
            conv3=L.Convolution2D(ch, out_size, 1, 1, 0, w, nobias=True),
            bn3=L.BatchNormalization(out_size),

            conv4=L.Convolution2D(in_size, out_size, 1, stride, 0, w, nobias=True),
            bn4=L.BatchNormalization(out_size),
        )

    def __call__(self, x, train):
        h1 = F.relu(self.bn1(self.conv1(x), test=not train))
        h1 = F.relu(self.bn2(self.conv2(h1), test=not train))
        h1 = self.bn3(self.conv3(h1), test=not train)
        h2 = self.bn4(self.conv4(x), test=not train)

        return F.relu(h1 + h2)


class BottleNeckB(chainer.Chain):
    def __init__(self, in_size, ch):
        w = math.sqrt(2)
        super(BottleNeckB, self).__init__(
            conv1=L.Convolution2D(in_size, ch, 1, 1, 0, w, nobias=True),
            bn1=L.BatchNormalization(ch),
            conv2=L.Convolution2D(ch, ch, 3, 1, 1, w, nobias=True),
            bn2=L.BatchNormalization(ch),
            conv3=L.Convolution2D(ch, in_size, 1, 1, 0, w, nobias=True),
            bn3=L.BatchNormalization(in_size),
        )

    def __call__(self, x, train):
        h = F.relu(self.bn1(self.conv1(x), test=not train))
        h = F.relu(self.bn2(self.conv2(h), test=not train))
        h = self.bn3(self.conv3(h), test=not train)

        return F.relu(h + x)


class Block(chainer.Chain):
    def __init__(self, layer, in_size, ch, out_size, stride=2):
        super(Block, self).__init__()
        links = [('a', BottleNeckA(in_size, ch, out_size, stride))]
        for i in range(layer-1):
            links += [('b{}'.format(i+1), BottleNeckB(out_size, ch))]

        for l in links:
            self.add_link(*l)
        self.forward = links

    def __call__(self, x, train):
        for name, _ in self.forward:
            f = getattr(self, name)
            x = f(x, train)

        return x

class ResNet(chainer.Chain):

    def __init__(self, no_label):
        w = math.sqrt(2)
        super(ResNet, self).__init__(
            conv1=L.Convolution2D(3, 64, 7, 2, 3, w, nobias=True),
            bn1=L.BatchNormalization(64),
            res2=Block(3, 64, 64, 256, 1),
            res3=Block(4, 256, 128, 512),
            res4=Block(6, 512, 256, 1024),
            res5=Block(3, 1024, 512, 2048),
            fc=L.Linear(None, no_label),
        )
        self.train = True

    def clear(self):
        self.loss = None
        self.accuracy = None

    def __call__(self, x):
        self.clear()
        h = self.bn1(self.conv1(x), test=not self.train)
        h = F.max_pooling_2d(F.relu(h), 3, stride=2)
        h = self.res2(h, self.train)
        h = self.res3(h, self.train)
        h = self.res4(h, self.train)
        h = self.res5(h, self.train)
        h = F.average_pooling_2d(h, 7, stride=1)
        h = self.fc(h)

        return h
# ====================

def copy_model(src, dst):
    assert isinstance(src, chainer.Chain)
    assert isinstance(dst, chainer.Chain)
    for child in src.children():
        if child.name not in dst.__dict__: continue
        dst_child = dst[child.name]
        if type(child) != type(dst_child): continue
        if isinstance(child, chainer.Chain):
            copy_model(child, dst_child)
        if isinstance(child, chainer.Link):
            match = True
            for a, b in zip(child.namedparams(), dst_child.namedparams()):
                if a[0] != b[0]:
                    match = False
                    break
                if a[1].data.shape != b[1].data.shape:
                    match = False
                    break
            if not match:
                print ('Ignore %s because of parameter mismatch' % child.name)
                continue
            for a, b in zip(child.namedparams(), dst_child.namedparams()):
                b[1].data = a[1].data
            print ('Copy %s' % child.name)

# model_path = "ResNet50.model"
# func = caffe.CaffeFunction(model_path)
# print(dir(func.children))
# exit()
# alex = Alex(25)
# alex = SqueezeNet(25)
alex = ResNet(25)
# copy_model(func, alex)
alex.to_gpu()
classifier = L.Classifier(alex)

#optimizer = optimizers.MomentumSGD(lr=0.001)
optimizer = optimizers.Adam()
optimizer.use_cleargrads()
optimizer.setup(classifier)
optimizer.add_hook(chainer.optimizer.WeightDecay(1e-5))

home = "/ramdisk/data/cookpad/"

train_data = ImageData(home + 'clf_train_images_labeled', home + 'clf_train_master.tsv')
test_data = ImageData(home + 'clf_test_images', home + 'clf_test.tsv')

classifier, optimizer = train_val(train_data, classifier, optimizer)
p = predict(test_data, classifier) 
pd.DataFrame(p.argmax(axis=1)).to_csv(home + 'sample_submit.csv', header=None)
